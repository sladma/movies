# Movies

## Used libraris

- Compose - UI
- Room - database
- Retrofit - REST API
- Hilt - Dependency injection
- ExoPlayer - video player
- Coil - async image loading
- Timber - logger