package cz.sladecekmartin.movies

import cz.sladecekmartin.movies.api.MoviesApi
import cz.sladecekmartin.movies.db.MoviesRepository
import cz.sladecekmartin.movies.model.Movie
import java.net.UnknownHostException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoviesManagerImpl @Inject constructor(
    private val moviesApi: MoviesApi,
    private val moviesRepository: MoviesRepository,
) : MoviesManager {

    override suspend fun movies(category: String?): List<Movie> {
        return try {
            try {
                val fetchedMovies = moviesApi.allMovies().map {
                    Movie(
                        title = it.name,
                        imageUrl = it.iconUri,
                        manifestUri = it.manifestUri,
                        playable = it.drm.contains("DEMO_CLEAR"),
                        categories = it.features.joinToString("|"),
                    )
                }
                moviesRepository.replaceMovies(fetchedMovies)
            } catch (e: UnknownHostException) {
                // try to reload stored movies
            }

            val storedMovies = moviesRepository.movies(category)
            if (category == null && storedMovies.isEmpty()) {
                // no data in database, force user to reload
                throw MoviesManager.NoInternetConnectionException()
            }
            storedMovies
        } catch (e: MoviesManager.NoInternetConnectionException) {
            throw MoviesManager.NoInternetConnectionException()
        } catch (e: Exception) {
            throw MoviesManager.InternalErrorException(e)
        }
    }

}