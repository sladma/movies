package cz.sladecekmartin.movies.di

import android.content.Context
import androidx.room.Room
import com.google.gson.Gson
import cz.sladecekmartin.movies.BuildConfig
import cz.sladecekmartin.movies.MoviesManager
import cz.sladecekmartin.movies.MoviesManagerImpl
import cz.sladecekmartin.movies.api.MoviesApi
import cz.sladecekmartin.movies.db.MoviesDatabase
import cz.sladecekmartin.movies.db.MoviesRepository
import cz.sladecekmartin.movies.db.MoviesRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    @Provides
    fun gson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    fun okHttpClient(): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.addInterceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .header("X-Client-Version", BuildConfig.VERSION_NAME)
                .build()
            chain.proceed(request)
        }
        return clientBuilder.build()
    }

    @Singleton
    @Provides
    fun provideMoviesApi(
        okHttp: OkHttpClient,
        gson: Gson,
    ): MoviesApi {
        return Retrofit.Builder()
            .baseUrl("https://gist.githubusercontent.com/")
            .client(okHttp)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(MoviesApi::class.java)
    }

    @Provides
    fun androidConnectivityManager(@ApplicationContext applicationContext: Context): android.net.ConnectivityManager {
        return applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as android.net.ConnectivityManager
    }

    @Singleton
    @Provides
    fun moviesDatabase(@ApplicationContext applicationContext: Context): MoviesDatabase {
        return Room.databaseBuilder(applicationContext, MoviesDatabase::class.java, "movies")
            .build()
    }

}

@Module
@InstallIn(SingletonComponent::class)
abstract class ApplicationBindModule {

    @Binds
    abstract fun bindMoviesManager(moviesManagerImpl: MoviesManagerImpl): MoviesManager

    @Binds
    abstract fun bindMoviesRepository(moviesRepositoryImpl: MoviesRepositoryImpl): MoviesRepository

}
