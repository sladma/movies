package cz.sladecekmartin.movies

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.ui.StyledPlayerView
import dagger.hilt.android.AndroidEntryPoint

private const val VIDEO_MANIFEST_URI_EXTRA = "VIDEO_MANIFEST_URI_EXTRA"


@AndroidEntryPoint
class PlayerActivity : ComponentActivity() {

    companion object {
        fun newIntent(context: Context, videoManifestUri: String): Intent =
            Intent(context, PlayerActivity::class.java).apply {
                putExtra(
                    VIDEO_MANIFEST_URI_EXTRA, videoManifestUri
                )
            }
    }

    private lateinit var videoManifestUri: String
    private lateinit var playerView: StyledPlayerView
    private lateinit var player: ExoPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.player_activity)
        videoManifestUri = requireNotNull(
            intent.getStringExtra(VIDEO_MANIFEST_URI_EXTRA)
        ) { "Argument $VIDEO_MANIFEST_URI_EXTRA is missing." }
        playerView = findViewById(R.id.player_view)
    }

    override fun onStart() {
        super.onStart()
        if (Build.VERSION.SDK_INT > 23) {
            initializePlayer()
            playerView.onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT <= 23) {
            initializePlayer()
            playerView.onResume()
        }
    }

    override fun onPause() {
        super.onPause()
        if (Build.VERSION.SDK_INT <= 23) {
            playerView.onPause()
            releasePlayer()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Build.VERSION.SDK_INT > 23) {
            playerView.onPause()
            releasePlayer()
        }
    }

    private fun initializePlayer() {
        player = ExoPlayer.Builder(this).build()
        player.playWhenReady = true
        playerView.player = player

        val mediaItem = MediaItem.fromUri(videoManifestUri)
        player.setMediaItem(mediaItem)
        player.prepare()
    }

    private fun releasePlayer() {
        player.release()
        playerView.player = null
    }

}