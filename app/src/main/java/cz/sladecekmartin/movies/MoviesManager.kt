package cz.sladecekmartin.movies

import cz.sladecekmartin.movies.model.Movie

interface MoviesManager {

    class NoInternetConnectionException : Exception()

    class InternalErrorException(cause: Exception) : Exception(cause)

    @Throws(NoInternetConnectionException::class, InternalErrorException::class)
    suspend fun movies(category: String? = null): List<Movie>

}