package cz.sladecekmartin.movies.api.data

data class RemoteMovie(
    val name: String,
    val shortName: String,
    val iconUri: String,
    val manifestUri: String,
    val drm: List<String>,
    val features: List<String>
)