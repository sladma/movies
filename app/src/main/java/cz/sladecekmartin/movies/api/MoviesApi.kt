package cz.sladecekmartin.movies.api

import cz.sladecekmartin.movies.api.data.RemoteMovie
import retrofit2.http.GET

interface MoviesApi {

    @GET("nextsux/f6e0327857c88caedd2dab13affb72c1/raw/04441487d90a0a05831835413f5942d58026d321/videos.json")
    suspend fun allMovies(): List<RemoteMovie>

}