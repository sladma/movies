package cz.sladecekmartin.movies.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MovieDao {
    @Query("SELECT * FROM movie")
    fun getAll(): List<MovieEntity>

    @Query("SELECT * FROM movie WHERE categories LIKE '%' || :category || '%'")
    fun getAllWithCategory(category: String): List<MovieEntity>

    @Insert
    fun insertAll(movies: List<MovieEntity>)

    @Query("DELETE FROM movie")
    fun deleteAll()
}