package cz.sladecekmartin.movies.db

import cz.sladecekmartin.movies.model.Movie

interface MoviesRepository {

    suspend fun movies(category: String? = null): List<Movie>

    suspend fun replaceMovies(movies: List<Movie>)

}