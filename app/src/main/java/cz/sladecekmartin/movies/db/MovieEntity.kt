package cz.sladecekmartin.movies.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import cz.sladecekmartin.movies.model.Movie

@Entity(tableName = "movie")
data class MovieEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val title: String,
    val imageUrl: String,
    val manifestUri: String,
    val playable: Boolean,
    val categories: String,
)

fun Movie.toMovieEntity(): MovieEntity {
    return MovieEntity(
        0,
        title,
        imageUrl,
        manifestUri,
        playable,
        categories
    )
}

fun MovieEntity.toMovie(): Movie {
    return Movie(
        title,
        imageUrl,
        manifestUri,
        playable,
        categories,
    )
}