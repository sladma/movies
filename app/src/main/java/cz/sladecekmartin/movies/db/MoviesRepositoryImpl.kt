package cz.sladecekmartin.movies.db

import cz.sladecekmartin.movies.model.Movie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoviesRepositoryImpl @Inject constructor(
    private val moviesDatabase: MoviesDatabase
) : MoviesRepository {

    override suspend fun movies(category: String?): List<Movie> {
        return withContext(Dispatchers.IO) {
            if (category != null) {
                moviesDatabase.movieDao().getAllWithCategory(category)
            } else {
                moviesDatabase.movieDao().getAll()
            }.map { it.toMovie() }
        }
    }

    override suspend fun replaceMovies(movies: List<Movie>) {
        withContext(Dispatchers.IO) {
            moviesDatabase.movieDao().deleteAll()
            moviesDatabase.movieDao().insertAll(movies.map { it.toMovieEntity() })
        }
    }

}