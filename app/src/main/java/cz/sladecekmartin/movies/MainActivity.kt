package cz.sladecekmartin.movies

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.lifecycle.lifecycleScope
import cz.sladecekmartin.movies.ui.main.MainScreen
import cz.sladecekmartin.movies.ui.main.MainScreenViewModel
import cz.sladecekmartin.movies.ui.theme.MoviesTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val mainScreenViewModel: MainScreenViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MoviesTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    MainScreen(mainScreenViewModel)
                }
            }
        }

        lifecycleScope.launchWhenCreated {
            mainScreenViewModel.openVideoPlayer.collect { videoManifestUri ->
                this@MainActivity.startActivity(
                    PlayerActivity.newIntent(
                        this@MainActivity,
                        videoManifestUri
                    )
                )
            }
        }

    }

}