package cz.sladecekmartin.movies.ui.main

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import cz.sladecekmartin.movies.R

@Composable
fun MovieItem(
    title: String,
    imageUrl: String,
    playable: Boolean,
    onClick: () -> Unit,
) {
    Card(
        modifier = if (playable) Modifier.clickable {
            onClick()
        } else {
            Modifier
        },
        elevation = 8.dp
    ) {
        Box {
            Image(
                modifier = Modifier
                    .fillMaxSize()
                    .defaultMinSize(minHeight = 200.dp),
                painter = rememberAsyncImagePainter(imageUrl),
                contentDescription = null
            )
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .background(Color.Black.copy(0.5f))
                    .padding(16.dp)
                    .align(Alignment.BottomCenter),
                text = title,
                color = Color.White
            )
            Icon(
                modifier = Modifier
                    .padding(8.dp)
                    .background(MaterialTheme.colors.primary, RoundedCornerShape(20.dp))
                    .padding(8.dp)
                    .size(40.dp)
                    .align(Alignment.TopEnd),
                painter = painterResource(id = if (playable) R.drawable.ic_play else R.drawable.ic_drm),
                contentDescription = "Play",
                tint = MaterialTheme.colors.onPrimary,
            )
        }
    }
}