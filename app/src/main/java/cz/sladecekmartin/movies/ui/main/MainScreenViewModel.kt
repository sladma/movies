package cz.sladecekmartin.movies.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.sladecekmartin.movies.MoviesManager
import cz.sladecekmartin.movies.model.Movie
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

private const val TAG = "MainScreen"


@HiltViewModel
class MainScreenViewModel @Inject constructor(
    private val moviesManager: MoviesManager,
) : ViewModel() {

    sealed class State {
        object Loading : State()
        data class Content(val movies: List<Movie>) : State()
        object NoContent : State()
        object NoInternet : State()
        object InternalError : State()
    }

    private val stateFlow = MutableStateFlow<State>(State.Loading)
    val state: StateFlow<State> = stateFlow

    private val openVideoPlayerFlow = MutableSharedFlow<String>(
        extraBufferCapacity = 1,
        onBufferOverflow = BufferOverflow.DROP_LATEST
    )
    val openVideoPlayer: Flow<String> = openVideoPlayerFlow

    private var reloadJob: Job? = null

    init {
        refresh()
    }

    fun onMovieClicked(manifestUri: String) {
        openVideoPlayerFlow.tryEmit(manifestUri)
    }

    fun refresh(category: String? = null) {
        reloadJob?.cancel()
        reloadJob = viewModelScope.launch {
            try {
                stateFlow.tryEmit(State.Loading)
                val movies = moviesManager.movies(category)
                if (movies.isNotEmpty()) {
                    stateFlow.tryEmit(State.Content(movies))
                } else {
                    stateFlow.tryEmit(State.NoContent)
                }
            } catch (noInternetConnection: MoviesManager.NoInternetConnectionException) {
                Timber.tag(TAG).w(noInternetConnection)
                stateFlow.tryEmit(State.NoInternet)
            } catch (internalError: MoviesManager.InternalErrorException) {
                Timber.tag(TAG).e(internalError)
                stateFlow.tryEmit(State.InternalError)
            }
        }
    }

}