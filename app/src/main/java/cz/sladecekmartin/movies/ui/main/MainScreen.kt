package cz.sladecekmartin.movies.ui.main

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import cz.sladecekmartin.movies.R

@Composable
fun MainScreen(mainScreenViewModel: MainScreenViewModel) {
    Box {
        val state by mainScreenViewModel.state.collectAsState()
        when (val currentState = state) {
            MainScreenViewModel.State.Loading -> {
                CircularProgressIndicator(
                    modifier = Modifier
                        .size(50.dp)
                        .align(Alignment.Center)
                )
            }
            is MainScreenViewModel.State.Content -> {
                Column {
                    Row(modifier = Modifier.padding(8.dp)) {
                        FeatureButton(R.string.all) {
                            mainScreenViewModel.refresh()
                        }
                        Spacer(modifier = Modifier.width(8.dp))
                        FeatureButton(R.string.feature_mp4) {
                            mainScreenViewModel.refresh("DEMO_MP4")
                        }
                        Spacer(modifier = Modifier.width(8.dp))
                        FeatureButton(R.string.feature_subtitles) {
                            mainScreenViewModel.refresh("DEMO_SUBTITLES")
                        }
                    }
                    LazyColumn(
                        modifier = Modifier.fillMaxSize(),
                        contentPadding = PaddingValues(16.dp),
                        verticalArrangement = Arrangement.spacedBy(16.dp)
                    ) {
                        items(currentState.movies) { movie ->
                            MovieItem(movie.title, movie.imageUrl, movie.playable, onClick = {
                                mainScreenViewModel.onMovieClicked(movie.manifestUri)
                            })
                        }
                    }
                }
            }
            MainScreenViewModel.State.NoInternet -> {
                Info(
                    mainScreenViewModel,
                    R.drawable.ic_no_internet,
                    "No internet"
                )
            }
            MainScreenViewModel.State.InternalError -> {
                Info(
                    mainScreenViewModel,
                    R.drawable.ic_error,
                    "Internal error"
                )
            }
            MainScreenViewModel.State.NoContent -> {
                Info(
                    mainScreenViewModel,
                    R.drawable.ic_no_content,
                    "No content"
                )
            }
        }
    }
}

@Composable
fun BoxScope.Info(
    mainScreenViewModel: MainScreenViewModel,
    icon: Int,
    text: String,
) {
    Column(
        modifier = Modifier
            .wrapContentSize()
            .align(Alignment.Center)
            .clickable {
                mainScreenViewModel.refresh()
            },
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Image(
            modifier = Modifier
                .size(50.dp),
            painter = painterResource(id = icon),
            contentDescription = text
        )
        Text(text = text)
    }
}

@Composable
fun RowScope.FeatureButton(
    text: Int,
    onClick: () -> Unit
) {
    Button(
        modifier = Modifier.weight(1f),
        onClick = { onClick() }) {
        Text(text = stringResource(id = text))
    }
}