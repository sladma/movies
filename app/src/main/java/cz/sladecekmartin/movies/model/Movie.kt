package cz.sladecekmartin.movies.model

data class Movie(
    val title: String,
    val imageUrl: String,
    val manifestUri: String,
    val playable: Boolean,
    val categories: String,
)